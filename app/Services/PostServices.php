<?php

namespace App\Services;

use App\Models\Posts;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class PostServices
{
    public function getAllWithPaginate($perPage = null)
    {
        return Posts::with([
            'tags',
        ])->paginate($perPage);
    }

    public function storePost($data)
    {
        DB::beginTransaction();
        $item = Posts::create($data);

        if (isset($data['listTags']) && $data['listTags'] && $item) {
            $tags = array_map(function ($element) {
                return ['name' => $element];
            }, $data['listTags']);

            $saveTags = $item->tags()->createMany($tags);

            if ($saveTags) {
                DB::commit();
                return true;
            }
        }
        DB::rollBack();
        return false;
    }

    public function getSearchWithPaginate($search, $perPage = null)
    {
        return Posts::with('tags')->where('title', '=', $search)
            ->orWhereHas('tags', function ($query) use ($search) {
                $query->where('name', '=', $search);
            })->paginate($perPage);
    }
}
