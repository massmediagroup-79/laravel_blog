<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePosts extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2|max:500',
            'description' => 'min:2|max:1000',
            'listTags.*' => 'required|min:2|max:500',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'listTags.*' => 'The tag name must be at least 2 characters and max 500.',
        ];
    }


}
